import React, { Component } from "react";

import { connect } from "react-redux";
import { Switch, Route, Link, Redirect } from "react-router-dom";
import {
  Row,
  Col,
  Container,
  Form,
  Input,
  FormGroup,
  Label,
  FormText,
  Button
} from "reactstrap";
import Signup from "../login/signup";
import MyNav from "../../components/nav";

import _ from "lodash";
class Login extends Component {
  render() {
    const { user } = this.props;
    console.log(
      _.isUndefined(user) || (!user.token || !user.refreshToken)
        ? " <Redirect to=sadsd"
        : user
    );
    return (
      <React.Fragment>
        <Container className="margin-padding">
          <Form>
            <h1 className="center text-center">Sign In</h1>
            <FormGroup className="margin-padding">
              <Input type="email" name="email" id="email" placeholder="Email" />
            </FormGroup>
            <FormGroup className="margin-padding">
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="Password"
              />
            </FormGroup>
            <FormGroup check>
              <Label check>Forgot your Password?</Label>
            </FormGroup>

            <Container className="margin-padding">
              <Button color="primary" size="lg" block className="btn-login">
                LOG IN
              </Button>
              <Row>
                <Col xs="6" className="button-center btn-group">
                  <Button  size="lg" block className="btn-facebook">FACEBOOK</Button>
                </Col>

                <Col xs="6" className="button-center btn-group">
                  <Button    size="lg" block className="btn-google">GOOGLE</Button>
                </Col>
              </Row>
            </Container>
          </Form>
        </Container>
      </React.Fragment>
    );
  }
}

const LoginConnected = connect(({ user }) => ({ user }))(Login);

export default LoginConnected;
