import React, { Component } from "react";

import { connect } from "react-redux";
import { Switch, Route, Link, Redirect } from "react-router-dom";

import Login from "../login";
import Signup from "../login/signup";
import MyNav from "../../components/nav";
import MyFooter from "../../components/footer";

import _ from "lodash";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      butler_name: "name",
      isOpen: false,
      page: "explore    ",
      
    };
  }
  render() {
    const { user } = this.props;
    
    return (
      <React.Fragment>
        <MyNav />
        <div className="">
          {!user.token || !user.refreshToken ? <Redirect to="/login" /> : null}
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Signup} />
          </Switch>
        </div>
        <MyFooter selected={this.state.page} />
      </React.Fragment>
    );
  }
}

const HomeConnected = connect(({ user }) => ({ user }))(Home);

export default HomeConnected;
