import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { connect } from "react-redux";

import Home from "../home";

const App = props => {
    return (
      <React.Fragment>
        <Switch>
          <Route
            exact
            path="/"
            render={_ => (window.location = "/orders/done")}
          />
          <Route path="/:page" component={Home} />
        </Switch>
      </React.Fragment>
    );
  
}

export default App;
