import API from "../api";

const initialState = {
  token: localStorage.getItem("user_token"),
  refreshToken: localStorage.getItem("user_refreshToken"),
  user_info: JSON.parse(localStorage.getItem("user_info"))
};

API.defaults.headers.common["Authorization"] = `Bearer ${initialState.token}`;

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case "SET_USER":
      localStorage.setItem("user_token", action.token);
      localStorage.setItem("user_refreshToken", action.refreshToken);
      localStorage.setItem("user_info", JSON.stringify(action.user_info));
      return {
        ...state,
        token: action.token,
        refreshToken: action.refreshToken,
        user_info: action.user_info
      };

    case "SET_UER_TOKEN":
      localStorage.setItem("user_token", action.token);
      return {
        ...state,
        token: action.token
      };

    case "SET_UER_REFRESH_TOKEN":
      localStorage.setItem("user_refreshToken", action.refreshToken);
      return {
        ...state,
        refreshToken: action.refreshToken
      };
    case "REMOVE_USER_TOKENS":
      localStorage.removeItem("user_token");
      localStorage.removeItem("user_refreshToken");
      return {
        ...state,
        token: null,
        refreshToken: null
      };

    default:
      return state;
  }
};
