/**
 * Created by erikccoder on 18/5/2018.
 */

import React from "react";
import _ from "lodash";
import { Redirect } from "react-router";

export function getLoadingOrFail({ firstLoad, error }) {
  if (!firstLoad) {
    return (
      <div className="w-100 h-100 p-5 d-inline-block">
        <h1 className="text-center align-middle">Loading...</h1>
      </div>
    );
  } else if (error) {
    console.log(error); //console.log(JSON.stringify(error, null, "\t"));
  }
  return false;
}
export function toWeekDay(day) {
  switch (day) {
    case 0: {
      return "Sun";
      break;
    }
    case 1: {
      return "Mon";
      break;
    }
    case 2: {
      return "Tue";
      break;
    }
    case 3: {
      return "Web";
      break;
    }
    case 4: {
      return "Thu";
      break;
    }
    case 5: {
      return "Fri";
      break;
    }
    case 6: {
      return "Sat";
      break;
    }
    default: {
      return "";
    }
  }
}
export function parseSearch(search) {
  return _.chain(search)
    .replace("?", "") // a=b454&c=dhjjh&f=g6hksdfjlksd
    .split("&") // ["a=b454","c=dhjjh","f=g6hksdfjlksd"]
    .map(_.partial(_.split, _, "=", 2)) // [["a","b454"],["c","dhjjh"],["f","g6hksdfjlksd"]]
    .fromPairs() // {"a":"b454","c":"dhjjh","f":"g6hksdfjlksd"}
    .value();
}
