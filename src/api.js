import axios from "axios";
import _ from "lodash";

const instance = axios.create({
  baseURL: `http://localhost:3333/api`
});

instance.interceptors.request.use(config => {
  return config;
});

instance.interceptors.response.use(
  response => response,
  ({ config, request, response }) => {
    const { name, status } = _.get(response, "data.error", {});

    if (status == 401 && name == "ExpiredJwtToken") {
      console.log("AUTO refresh token");
      if (window.confirm(`${status}:${name}. Goto login page?`)) {
        localStorage.removeItem("user_token");
        localStorage.removeItem("user_refreshToken");
        window.location = "/login";
      }
      // return (
      // 	instance.put("/login", {
      // 		refreshToken: localStorage.getItem("user_refreshToken")
      // 	})
      // )
    } else {
      console.log("need to login");
    }

    return Promise.reject({ config, request, response });
  }
);

export default instance;
