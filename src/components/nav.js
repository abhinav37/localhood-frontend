import React, { Component } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import history from "history/createBrowserHistory";
import { connect } from "react-redux";

class MyNav extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    let name = "Butler";
    let link1 = false;
    let link2 = false;
    if (props.user && props.user.user_info)
      name = props.user.user_info.full_name.split(" ")[0];
    if (props.link) {
      switch (props.link) {
        case "New Orders": {
          link1 = true;
          break;
        }
        case "Orders Done": {
          link2 = true;
          break;
        }
      }
    }
    this.state = {
      butler_name: name,
      isOpen: false,
      selected: "New Order",
      link1,
      link2
    };
  }
  logout() {
    localStorage.removeItem("user_token");
    localStorage.removeItem("user_refreshToken");
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <Navbar color="light" light expand="md" className="zerotoppadding">
        <NavbarBrand href="/profile">
            <img src={`${process.env.PUBLIC_URL}/img/localhood-logo/localhood-logo.png`}/>{' '}Localhood
        </NavbarBrand>
        <Nav className="ml-auto light" horizontal>
             <NavItem active={this.state.link2}>
            <NavLink href="/account"><img src={`${process.env.PUBLIC_URL}/img/account/account.png`}/></NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}
const MyNavConnected = connect(({ user }) => ({ user }))(MyNav);

export default MyNavConnected;