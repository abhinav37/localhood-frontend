import React, { Component } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Container,
  Row,
  Col,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import history from "history/createBrowserHistory";
import { connect } from "react-redux";

class MyFooter extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    let name = "Butler";
    let link1 = false;
    let link2 = false;
    if (props.user && props.user.user_info)
      name = props.user.user_info.full_name.split(" ")[0];
    if (props.link) {
      switch (props.link) {
        case "New Orders": {
          link1 = true;
          break;
        }
        case "Orders Done": {
          link2 = true;
          break;
        }
      }
    }
    this.state = {
      butler_name: name,
      isOpen: false,
      selected: "home",
      link1,
      link2
    };
  }
  logout() {
    localStorage.removeItem("user_token");
    localStorage.removeItem("user_refreshToken");
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  isSelected = selected => {
    return this.props.selected == selected
      ? "rounded-circle circle-selected"
      : "rounded-circle";
  };
  render() {
    return (
      <Navbar bottom color="light" light expand="md" className="fixed-bottom">
        <Container>
          <Col xs="auto" className={this.isSelected("home")}>
            <img src={`${process.env.PUBLIC_URL}/img/home/home.png`} />
          </Col>
          <Col xs="auto" className={this.isSelected("calendar")}>
            <img src={`${process.env.PUBLIC_URL}/img/calendar/calendar.png`} />
          </Col>
          <Col xs="auto" className={this.isSelected("discount")}>
            <img src={`${process.env.PUBLIC_URL}/img/discount/discount.png`} />
          </Col>
          <Col xs="auto" className={this.isSelected("explore")}>
            <img src={`${process.env.PUBLIC_URL}/img/explore/explore.png`} />
          </Col>
          <Col xs="auto" className={this.isSelected("facebook")}>
            <img src={`${process.env.PUBLIC_URL}/img/facebook/facebook.png`} />
          </Col>
        </Container>
      </Navbar>
    );
  }
}
const MyFooterConnected = connect(({ user }) => ({ user }))(MyFooter);

export default MyFooterConnected;
